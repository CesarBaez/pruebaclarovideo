package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Profile{

	@SerializedName("videotype")
	private String videotype;

	@SerializedName("screenformat")
	private String screenformat;

	@SerializedName("audiotype")
	private String audiotype;

	@SerializedName("hd")
	private Hd hd;

	public void setVideotype(String videotype){
		this.videotype = videotype;
	}

	public String getVideotype(){
		return videotype;
	}

	public void setScreenformat(String screenformat){
		this.screenformat = screenformat;
	}

	public String getScreenformat(){
		return screenformat;
	}

	public void setAudiotype(String audiotype){
		this.audiotype = audiotype;
	}

	public String getAudiotype(){
		return audiotype;
	}

	public void setHd(Hd hd){
		this.hd = hd;
	}

	public Hd getHd(){
		return hd;
	}
}