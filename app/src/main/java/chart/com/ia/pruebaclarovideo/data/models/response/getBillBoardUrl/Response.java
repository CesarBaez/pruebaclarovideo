package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;


import com.google.gson.annotations.SerializedName;


public class Response{

	@SerializedName("modules")
	private Modules modules;

	public void setModules(Modules modules){
		this.modules = modules;
	}

	public Modules getModules(){
		return modules;
	}
}