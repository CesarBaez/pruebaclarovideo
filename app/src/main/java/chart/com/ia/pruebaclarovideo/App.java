package chart.com.ia.pruebaclarovideo;

import android.app.Application;
import android.support.multidex.MultiDexApplication;

import chart.com.ia.pruebaclarovideo.data.DataConfiguration;
import chart.com.ia.pruebaclarovideo.di.data.DataModule;
import chart.com.ia.pruebaclarovideo.di.main.components.ApplicationComponent;
import chart.com.ia.pruebaclarovideo.di.main.components.DaggerApplicationComponent;
import chart.com.ia.pruebaclarovideo.di.main.modules.ApplicationModule;

public class App extends MultiDexApplication {

    private static ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    private void initDagger() {
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .dataModule(new DataModule(DataConfiguration.BASE_URL))
                .build();
    }

    public static ApplicationComponent getApplicationComponents() {
        return applicationComponent;
    }
}
