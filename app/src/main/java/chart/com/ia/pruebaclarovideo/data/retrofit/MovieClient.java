package chart.com.ia.pruebaclarovideo.data.retrofit;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieRetrofitService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MovieClient implements MovieService {
    private MovieRetrofitService service;

    public MovieClient(MovieRetrofitService service) {
        this.service = service;
    }

    @Override
    public Observable<MovieResponse> getMovie(String url) {
        return service.getMovie(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
