package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class UniversalId{

	@SerializedName("content_providers")
	private List<ContentProvidersItem> contentProviders;

	@SerializedName("metadata_providers")
	private MetadataProviders metadataProviders;

	public void setContentProviders(List<ContentProvidersItem> contentProviders){
		this.contentProviders = contentProviders;
	}

	public List<ContentProvidersItem> getContentProviders(){
		return contentProviders;
	}

	public void setMetadataProviders(MetadataProviders metadataProviders){
		this.metadataProviders = metadataProviders;
	}

	public MetadataProviders getMetadataProviders(){
		return metadataProviders;
	}
}