package chart.com.ia.pruebaclarovideo.data.retrofit;

import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ResponseGetUrl;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardRetrofitService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardService;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BillBoardClient implements BillBoardService {

    private BillBoardRetrofitService service;

    public BillBoardClient(BillBoardRetrofitService service) {
        this.service = service;
    }

    @Override
    public Observable<ResponseGetUrl> getBillBoardUrl() {
        return service.getBillBoardUrl()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBillBoard> getBillBoard(String url) {
        return service.getBillBoard(url)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
