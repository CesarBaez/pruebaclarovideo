package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class TalentsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("internal_ids")
	private List<Integer> internalIds;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("id")
	private String id;

	@SerializedName("first_name")
	private String firstName;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setInternalIds(List<Integer> internalIds){
		this.internalIds = internalIds;
	}

	public List<Integer> getInternalIds(){
		return internalIds;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}
}