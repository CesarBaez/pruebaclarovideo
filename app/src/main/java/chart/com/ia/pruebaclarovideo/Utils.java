package chart.com.ia.pruebaclarovideo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.Common;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.GenreItem;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.Genres;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.RoleItem;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.Roles;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.TalentItem;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.Talents;

public class Utils {
    public static void putImageOnView(ImageView imageView, String url, Context context) {
        Picasso.with(context)
                .load(String.format("%s", url))
                // .placeholder(R.drawable.placeholder_image)
                .into(imageView);
    }

    public static String getSinopsisString(Common common) {
        return common.getTitle() + "\n" +
                "(" + common.getExtendedcommon().getMedia().getPublishyear() + ")" +
                "   " + common.getDuration() + "\n\n" +
                "Generos: " + getGenres(common.getExtendedcommon().getGenres()) + "\n\n" +
                common.getExtendedcommon().getMedia().getDescriptionExtended() + "\n\n" +
                "Titulo Original: " + common.getExtendedcommon().getMedia().getOriginaltitle();
    }

    public static String getGenres(Genres genres) {
        String strGenres = "";
        for (GenreItem genresItem : genres.getGenre()) {
            strGenres = strGenres + genresItem.getDesc() + ", ";
        }
        return strGenres.substring(0, strGenres.lastIndexOf(","));
    }

    public static String getReparto(Roles roles) {
        String strRoles = "\n";
        for (RoleItem roleItem : roles.getRole()) {
            strRoles=strRoles+roleItem.getDesc()+": "+getTalents(roleItem.getTalents())+"\n\n";
        }
        return strRoles;
    }

    public static String getTalents(Talents talents){
        String strTalents="";
        for (TalentItem talentItem: talents.getTalent())
        {
            strTalents=strTalents+talentItem.getName()+" "+talentItem.getSurname()+", ";
        }
        return strTalents.substring(0,strTalents.lastIndexOf(","));
    }
    public static Boolean isWifiConected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Boolean isMobileNetConected(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (info != null) {
                if (info.isConnected()) {
                    return true;
                }
            }
        }
        return false;
    }
}
