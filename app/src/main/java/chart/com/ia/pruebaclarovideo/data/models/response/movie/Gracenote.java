package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Gracenote{

	@SerializedName("duration")
	private String duration;

	@SerializedName("cast")
	private List<CastItem> cast;

	@SerializedName("publishyear")
	private String publishyear;

	@SerializedName("keywords")
	private List<KeywordsItem> keywords;

	@SerializedName("genres")
	private List<String> genres;

	@SerializedName("rating")
	private String rating;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("description")
	private String description;

	@SerializedName("official_url")
	private String officialUrl;

	@SerializedName("id")
	private String id;

	@SerializedName("provider_code")
	private String providerCode;

	@SerializedName("title")
	private String title;

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setCast(List<CastItem> cast){
		this.cast = cast;
	}

	public List<CastItem> getCast(){
		return cast;
	}

	public void setPublishyear(String publishyear){
		this.publishyear = publishyear;
	}

	public String getPublishyear(){
		return publishyear;
	}

	public void setKeywords(List<KeywordsItem> keywords){
		this.keywords = keywords;
	}

	public List<KeywordsItem> getKeywords(){
		return keywords;
	}

	public void setGenres(List<String> genres){
		this.genres = genres;
	}

	public List<String> getGenres(){
		return genres;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setOfficialUrl(String officialUrl){
		this.officialUrl = officialUrl;
	}

	public String getOfficialUrl(){
		return officialUrl;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setProviderCode(String providerCode){
		this.providerCode = providerCode;
	}

	public String getProviderCode(){
		return providerCode;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}
}