package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class ContentProvidersItem{

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("provider_id")
	private String providerId;

	@SerializedName("provider_code")
	private String providerCode;

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	public void setProviderId(String providerId){
		this.providerId = providerId;
	}

	public String getProviderId(){
		return providerId;
	}

	public void setProviderCode(String providerCode){
		this.providerCode = providerCode;
	}

	public String getProviderCode(){
		return providerCode;
	}
}