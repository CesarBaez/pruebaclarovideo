package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Hd{

	@SerializedName("detail")
	private String detail;

	@SerializedName("enabled")
	private String enabled;

	public void setDetail(String detail){
		this.detail = detail;
	}

	public String getDetail(){
		return detail;
	}

	public void setEnabled(String enabled){
		this.enabled = enabled;
	}

	public String getEnabled(){
		return enabled;
	}
}