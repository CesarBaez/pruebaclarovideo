package chart.com.ia.pruebaclarovideo.ui.BillBoard;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebaclarovideo.App;
import chart.com.ia.pruebaclarovideo.R;
import chart.com.ia.pruebaclarovideo.Utils;
import chart.com.ia.pruebaclarovideo.adapters.BillBoardAdapter;
import chart.com.ia.pruebaclarovideo.data.DataConfiguration;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.GroupsItem;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.ui.MovieDetail.MovieDetailActivity;
import ia.com.commons.view.BaseActivity;
import rx.Observable;

public class BillBoardActivity extends BaseActivity implements BillBoardPresenter.View, BillBoardAdapter.onMovieClickListener, SearchView.OnQueryTextListener {

    @BindView(R.id.status)
    TextView tvStatus;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.recycler_billboard)
    RecyclerView rvBillBoard;
    @Inject
    Context context;
    @Inject
    Gson gson;
    @Inject
    BillBoardPresenter presenter;
    SearchView searchView = null;

    private ResponseBillBoard billBoardObj;
    List<GroupsItem> groupsItems = new ArrayList<>();
    private String baseUrl;
    public static final String KEY_URL_EXTRA = "key_url_extra";


    @Override
    protected void initActivity() {
        super.initActivity();
        initDagger();
        tvStatus.setVisibility(View.GONE);
        hideLoading();

    }

    public void initRecyclerView() {
        BillBoardAdapter adapter = new BillBoardAdapter(context, groupsItems, this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        rvBillBoard.setLayoutManager(layoutManager);
        rvBillBoard.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_buscador, menu);

        MenuItem searchItem = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) BillBoardActivity.this.getSystemService(Context.SEARCH_SERVICE);

        // SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        if (searchView != null) {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(BillBoardActivity.this.getComponentName()));
        }

        assert searchView != null;
        searchView.setOnQueryTextListener(this);


        return super.onCreateOptionsMenu(menu);
    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    private Boolean validateInternet(){
        return (Utils.isMobileNetConected(this)||Utils.isWifiConected(this));
    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        if (validateInternet()){
            presenter.setView(this);
            presenter.getBillBoardUrl();
        }
        else{
            setOffline();
        }
    }
    private void setOffline(){
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(getResources().getString(R.string.sin_conexion));
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_bill_board;
    }

    @Override
    public void onBillBoardServiceSuccess(ResponseBillBoard billBoardResponse, String baseUrl) {
        billBoardObj = billBoardResponse;
        this.baseUrl = baseUrl;
        groupsItems = billBoardObj.getResponse().getGroups();
        initRecyclerView();
    }

    @Override
    public void onBillBoardServiceError(String error) {
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(error);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onMovieClicked(String moviesItem) {
        Intent intent = new Intent(BillBoardActivity.this, MovieDetailActivity.class);
        intent.putExtra(KEY_URL_EXTRA, DataConfiguration.Movie.GET_MOVIE + baseUrl + "&group_id=" + moviesItem);
        startActivity(intent);
    }


    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<GroupsItem> groupsItems = new ArrayList<>();
        Observable.from(billBoardObj.getResponse().getGroups())
                .filter(groupsItem -> groupsItem.getTitle().toLowerCase().contains(newText.toLowerCase()))
                .subscribe(groupsItems::add);
        if (groupsItems.size() == 0) {
          tvStatus.setVisibility(View.VISIBLE);
          tvStatus.setText(getResources().getString(R.string.no_hay_resultados)+" "+newText);
        }else {
            tvStatus.setVisibility(View.GONE);
        }
        this.groupsItems = groupsItems;
        initRecyclerView();
        return true;
    }
}
