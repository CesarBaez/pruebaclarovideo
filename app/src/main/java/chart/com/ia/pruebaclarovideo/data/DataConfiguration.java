package chart.com.ia.pruebaclarovideo.data;

public class DataConfiguration {
    public static final String BASE_URL = "https://mfwkweb-api.clarovideo.net";

    private String baseUrl;

    public DataConfiguration(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public static class BillBoard{
        public static  final String GET_BILLBOARD_URL="/services/cms/level?api_version=v5.5&authpn=webclient&authpt=tfg1h3j4k6fd7&format=json&region=mexico&device_id=web&device_category=web&device_model=web&device_type=web&device_manufacturer=generic&HKS=9lrk9et93ck595t27qav0mkcf1&node=sapelcienficc";
        public static  final String GET_BILLBOARD="content/list";
    }
    public static class Movie{
        public static  final String GET_MOVIE="/services/content/data?";
    }

}
