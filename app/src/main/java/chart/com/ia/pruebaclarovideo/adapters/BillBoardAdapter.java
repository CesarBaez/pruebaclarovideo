package chart.com.ia.pruebaclarovideo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import chart.com.ia.pruebaclarovideo.R;
import chart.com.ia.pruebaclarovideo.Utils;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.GroupsItem;

public class BillBoardAdapter extends RecyclerView.Adapter<BillBoardAdapter.ViewHolder> {

    private Context context;
    private List<GroupsItem> movies;
    private onMovieClickListener listener;


    public BillBoardAdapter(Context context, List<GroupsItem> movies, onMovieClickListener listener) {
        this.context = context;
        this.movies = movies;
        this.listener = listener;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cartel_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GroupsItem item = movies.get(position);
        holder.tvTitle.setText(item.getTitle());
        Utils.putImageOnView(holder.ivCartel,item.getImageSmall(),context);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMovieClicked(item.getId());
            }
        });
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvInformation, tvGender;
        ImageView ivCartel;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_movie_title);
            ivCartel = (ImageView) itemView.findViewById(R.id.imagen_cartel);
        }
    }

    public interface onMovieClickListener {
        void onMovieClicked(String moviesItem);
    }
}