package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Genres{

	@SerializedName("genre")
	private List<GenreItem> genre;

	public void setGenre(List<GenreItem> genre){
		this.genre = genre;
	}

	public List<GenreItem> getGenre(){
		return genre;
	}
}