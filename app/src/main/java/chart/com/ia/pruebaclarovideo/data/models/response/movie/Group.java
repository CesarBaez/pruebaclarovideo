package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Group{

	@SerializedName("external")
	private External external;

	@SerializedName("common")
	private Common common;

	@SerializedName("universal_id")
	private UniversalId universalId;

	public void setExternal(External external){
		this.external = external;
	}

	public External getExternal(){
		return external;
	}

	public void setCommon(Common common){
		this.common = common;
	}

	public Common getCommon(){
		return common;
	}

	public void setUniversalId(UniversalId universalId){
		this.universalId = universalId;
	}

	public UniversalId getUniversalId(){
		return universalId;
	}
}