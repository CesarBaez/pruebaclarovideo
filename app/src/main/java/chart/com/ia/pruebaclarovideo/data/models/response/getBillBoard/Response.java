package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Response{

	@SerializedName("total")
	private int total;

	@SerializedName("groups")
	private List<GroupsItem> groups;

	public void setTotal(int total){
		this.total = total;
	}

	public int getTotal(){
		return total;
	}

	public void setGroups(List<GroupsItem> groups){
		this.groups = groups;
	}

	public List<GroupsItem> getGroups(){
		return groups;
	}
}