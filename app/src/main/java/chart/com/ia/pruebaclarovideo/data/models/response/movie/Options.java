package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Options{

	@SerializedName("count")
	private int count;

	@SerializedName("option")
	private List<OptionItem> option;

	public void setCount(int count){
		this.count = count;
	}

	public int getCount(){
		return count;
	}

	public void setOption(List<OptionItem> option){
		this.option = option;
	}

	public List<OptionItem> getOption(){
		return option;
	}
}