package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class CastItem{

	@SerializedName("role_name")
	private String roleName;

	@SerializedName("role_id")
	private String roleId;

	@SerializedName("talents")
	private List<TalentsItem> talents;

	public void setRoleName(String roleName){
		this.roleName = roleName;
	}

	public String getRoleName(){
		return roleName;
	}

	public void setRoleId(String roleId){
		this.roleId = roleId;
	}

	public String getRoleId(){
		return roleId;
	}

	public void setTalents(List<TalentsItem> talents){
		this.talents = talents;
	}

	public List<TalentsItem> getTalents(){
		return talents;
	}
}