package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class KeywordsItem{

	@SerializedName("keys")
	private List<String> keys;

	@SerializedName("type")
	private String type;

	public void setKeys(List<String> keys){
		this.keys = keys;
	}

	public List<String> getKeys(){
		return keys;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}