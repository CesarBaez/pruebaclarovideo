package chart.com.ia.pruebaclarovideo.ui.MovieDetail;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import chart.com.ia.pruebaclarovideo.App;
import chart.com.ia.pruebaclarovideo.R;
import chart.com.ia.pruebaclarovideo.Utils;
import chart.com.ia.pruebaclarovideo.adapters.SectionsPagerAdapter;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.Common;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.GenreItem;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.Genres;
import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import chart.com.ia.pruebaclarovideo.ui.BillBoard.BillBoardActivity;
import ia.com.commons.view.BaseActivity;


public class MovieDetailActivity extends BaseActivity implements MoviePresenter.View {

    @BindView(R.id.status)
    TextView tvStatus;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.imagen_bg)
    ImageView imageBg;
    @BindView(R.id.imagen_cartel_detalle)
    ImageView imageCartel;

    @Inject
    MoviePresenter moviePresenter;

    private MovieResponse movieResponse;

    private String url = "";
    private List<Fragment> fragments = new ArrayList<>();
    private ViewPager mViewPager;
    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_movie_detail;
    }

    @Override
    protected void initActivity() {
        super.initActivity();
        initDagger();
        if (this.getIntent() != null) {
            if (this.getIntent().getExtras() != null) {
                url = this.getIntent().getExtras().getString(BillBoardActivity.KEY_URL_EXTRA);
            }
        }
        tvStatus.setVisibility(View.GONE);
    }

    @Override
    protected void initPresenter() {
        super.initPresenter();
        if (validateInternet()){
            moviePresenter.setView(this);
            moviePresenter.getMovie(url);
        }else {
            setOffline();
        }


    }

    private void initDagger() {
        App.getApplicationComponents().inject(this);
    }

    private Boolean validateInternet(){
        return (Utils.isMobileNetConected(this)||Utils.isWifiConected(this));
    }
    private void setOffline(){
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(getResources().getString(R.string.sin_conexion));
    }

    @Override
    public void onMovieServiceSuccess(MovieResponse movieResponse) {
        this.movieResponse = movieResponse;
        initViews();
    }

    private void initViews() {
        Utils.putImageOnView(imageBg, movieResponse.getResponse().getGroup().getCommon().getImageBackground(), this);
        Utils.putImageOnView(imageCartel, movieResponse.getResponse().getGroup().getCommon().getImageLarge(), this);
        this.setTitle(movieResponse.getResponse().getGroup().getCommon().getTitle());

        fragments.add(PlaceholderFragment.newInstance(true, Utils.getSinopsisString(movieResponse.getResponse().getGroup().getCommon())));
        fragments.add(PlaceholderFragment.newInstance(false, Utils.getReparto(movieResponse.getResponse().getGroup().getCommon().getExtendedcommon().getRoles())));

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), fragments);
        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.pagerDetalle);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
    }


    @Override
    public void onMovieServiceError(String error) {
        tvStatus.setVisibility(View.VISIBLE);
        tvStatus.setText(error);
    }

    @Override
    public void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }
}
