package chart.com.ia.pruebaclarovideo.ui.BillBoard;

import javax.inject.Inject;

import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ComponentItem;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.Entry;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ModuleItem;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.Response;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ResponseGetUrl;
import chart.com.ia.pruebaclarovideo.domain.BillBoardInteractor;
import ia.com.commons.view.BasePresenter;

public class BillBoardPresenter extends BasePresenter<BillBoardPresenter.View> implements BillBoardInteractor.onBillBoardListener {

    private BillBoardInteractor billBoardInteractor;
    private String baseUrl;

    public void getBillBoard(String url, String api_level) {
        billBoardInteractor.getBillBoard(url, api_level);

    }

    public void getBillBoardUrl() {
        if (isViewAttached()) {
            billBoardInteractor.getBillBoardUrl();
            getView().showLoading();
        }
    }

    @Inject
    public BillBoardPresenter(BillBoardInteractor billBoardInteractor) {
        this.billBoardInteractor = billBoardInteractor;
        this.billBoardInteractor.setListener(this);
    }

    @Override
    public void onServiceError(String mensaje) {
        if (isViewAttached()) {
            getView().hideLoading();
            getView().onBillBoardServiceError(mensaje);
        }

    }

    @Override
    public void onDataSent(ResponseBillBoard billBoardResponse) {
        billBoardResponse.getResponse();
        if (isViewAttached()) {
            getView().hideLoading();
            getView().onBillBoardServiceSuccess(billBoardResponse, baseUrl);
        }
    }

    @Override
    public void onGetUrlSuccess(ResponseGetUrl responseGetUrl) {
        getBillBoard(getUrlString(responseGetUrl.getResponse()), baseUrl = getApiLevel(responseGetUrl.getEntry()));
    }

    private String getUrlString(Response response) {
        for (ModuleItem item : response.getModules().getModule()) {
            if (item.getType().equals("listadoinfinito")) {
                for (ComponentItem componentItem : item.getComponents().getComponent()) {
                    if (componentItem.getType().equals("Listadoinfinito")) {
                        return componentItem.getProperties().getUrl();
                    }
                }
            }
        }
        return "";
    }

    private String getApiLevel(Entry entry) {
        return "api_version=" + entry.getApiVersion() +
                "&authpn=" + entry.getAuthpn() +
                "&authpt=" + entry.getAuthpt() +
                "&format=" + entry.getFormat() +
                "&region=" + entry.getRegion() +
                "&devirece_id=" + entry.getDeviceId() +
                "&device_category=" + entry.getDeviceCategory() +
                "&device_model=" + entry.getDeviceModel() +
                "&device_type=" + entry.getDeviceType() +
                "&device_manufacturer=" + entry.getDeviceManufacturer() +
                "&HKS=" + entry.getHKS();
    }

    public interface View extends BasePresenter.View {
        void onBillBoardServiceSuccess(ResponseBillBoard billBoardResponse, String baseUrl);

        void onBillBoardServiceError(String error);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
