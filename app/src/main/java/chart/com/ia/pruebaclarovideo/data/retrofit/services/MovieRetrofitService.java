package chart.com.ia.pruebaclarovideo.data.retrofit.services;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface MovieRetrofitService {
    @GET()
    Observable<MovieResponse> getMovie(
            @Url String url
    );
}
