package chart.com.ia.pruebaclarovideo.data.retrofit.services;

import chart.com.ia.pruebaclarovideo.data.DataConfiguration;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ResponseGetUrl;
import retrofit2.http.GET;
import retrofit2.http.Url;
import rx.Observable;

public interface BillBoardRetrofitService {
    @GET(DataConfiguration.BillBoard.GET_BILLBOARD_URL)
    Observable<ResponseGetUrl> getBillBoardUrl();

    @GET()
    Observable<ResponseBillBoard> getBillBoard(
            @Url String url);
}
