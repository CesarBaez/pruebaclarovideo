package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Ranking{

	@SerializedName("votes_count")
	private int votesCount;

	@SerializedName("average_votes")
	private int averageVotes;

	@SerializedName("views_count")
	private int viewsCount;

	public void setVotesCount(int votesCount){
		this.votesCount = votesCount;
	}

	public int getVotesCount(){
		return votesCount;
	}

	public void setAverageVotes(int averageVotes){
		this.averageVotes = averageVotes;
	}

	public int getAverageVotes(){
		return averageVotes;
	}

	public void setViewsCount(int viewsCount){
		this.viewsCount = viewsCount;
	}

	public int getViewsCount(){
		return viewsCount;
	}
}