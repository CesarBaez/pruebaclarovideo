package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Language{

	@SerializedName("original")
	private Original original;

	@SerializedName("dubbed")
	private String dubbed;

	@SerializedName("options")
	private Options options;

	@SerializedName("subbed")
	private String subbed;

	public void setOriginal(Original original){
		this.original = original;
	}

	public Original getOriginal(){
		return original;
	}

	public void setDubbed(String dubbed){
		this.dubbed = dubbed;
	}

	public String getDubbed(){
		return dubbed;
	}

	public void setOptions(Options options){
		this.options = options;
	}

	public Options getOptions(){
		return options;
	}

	public void setSubbed(String subbed){
		this.subbed = subbed;
	}

	public String getSubbed(){
		return subbed;
	}
}