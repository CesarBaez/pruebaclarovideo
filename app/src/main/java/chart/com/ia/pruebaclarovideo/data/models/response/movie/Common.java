package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Common{

	@SerializedName("date")
	private String date;

	@SerializedName("image_small_alt")
	private String imageSmallAlt;

	@SerializedName("image_sprites")
	private String imageSprites;

	@SerializedName("image_medium")
	private String imageMedium;

	@SerializedName("image_clean_horizontal")
	private String imageCleanHorizontal;

	@SerializedName("description")
	private String description;

	@SerializedName("large_description")
	private String largeDescription;

	@SerializedName("title")
	private String title;

	@SerializedName("duration")
	private String duration;

	@SerializedName("extendedcommon")
	private Extendedcommon extendedcommon;

	@SerializedName("image_still")
	private String imageStill;

	@SerializedName("media_type")
	private String mediaType;

	@SerializedName("id")
	private String id;

	@SerializedName("image_background")
	private String imageBackground;

	@SerializedName("image_frames")
	private String imageFrames;

	@SerializedName("image_clean_vertical")
	private String imageCleanVertical;

	@SerializedName("image_medium_alt")
	private String imageMediumAlt;

	@SerializedName("image_small")
	private String imageSmall;

	@SerializedName("image_base_square")
	private String imageBaseSquare;

	@SerializedName("image_base_horizontal")
	private String imageBaseHorizontal;

	@SerializedName("ranking")
	private Ranking ranking;

	@SerializedName("position")
	private int position;

	@SerializedName("image_base_vertical")
	private String imageBaseVertical;

	@SerializedName("image_large")
	private String imageLarge;

	@SerializedName("title_uri")
	private String titleUri;

	@SerializedName("image_clean_square")
	private String imageCleanSquare;

	@SerializedName("image_large_alt")
	private String imageLargeAlt;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setImageSmallAlt(String imageSmallAlt){
		this.imageSmallAlt = imageSmallAlt;
	}

	public String getImageSmallAlt(){
		return imageSmallAlt;
	}

	public void setImageSprites(String imageSprites){
		this.imageSprites = imageSprites;
	}

	public String getImageSprites(){
		return imageSprites;
	}

	public void setImageMedium(String imageMedium){
		this.imageMedium = imageMedium;
	}

	public String getImageMedium(){
		return imageMedium;
	}

	public void setImageCleanHorizontal(String imageCleanHorizontal){
		this.imageCleanHorizontal = imageCleanHorizontal;
	}

	public String getImageCleanHorizontal(){
		return imageCleanHorizontal;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setLargeDescription(String largeDescription){
		this.largeDescription = largeDescription;
	}

	public String getLargeDescription(){
		return largeDescription;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setExtendedcommon(Extendedcommon extendedcommon){
		this.extendedcommon = extendedcommon;
	}

	public Extendedcommon getExtendedcommon(){
		return extendedcommon;
	}

	public void setImageStill(String imageStill){
		this.imageStill = imageStill;
	}

	public String getImageStill(){
		return imageStill;
	}

	public void setMediaType(String mediaType){
		this.mediaType = mediaType;
	}

	public String getMediaType(){
		return mediaType;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setImageBackground(String imageBackground){
		this.imageBackground = imageBackground;
	}

	public String getImageBackground(){
		return imageBackground;
	}

	public void setImageFrames(String imageFrames){
		this.imageFrames = imageFrames;
	}

	public String getImageFrames(){
		return imageFrames;
	}

	public void setImageCleanVertical(String imageCleanVertical){
		this.imageCleanVertical = imageCleanVertical;
	}

	public String getImageCleanVertical(){
		return imageCleanVertical;
	}

	public void setImageMediumAlt(String imageMediumAlt){
		this.imageMediumAlt = imageMediumAlt;
	}

	public String getImageMediumAlt(){
		return imageMediumAlt;
	}

	public void setImageSmall(String imageSmall){
		this.imageSmall = imageSmall;
	}

	public String getImageSmall(){
		return imageSmall;
	}

	public void setImageBaseSquare(String imageBaseSquare){
		this.imageBaseSquare = imageBaseSquare;
	}

	public String getImageBaseSquare(){
		return imageBaseSquare;
	}

	public void setImageBaseHorizontal(String imageBaseHorizontal){
		this.imageBaseHorizontal = imageBaseHorizontal;
	}

	public String getImageBaseHorizontal(){
		return imageBaseHorizontal;
	}

	public void setRanking(Ranking ranking){
		this.ranking = ranking;
	}

	public Ranking getRanking(){
		return ranking;
	}

	public void setPosition(int position){
		this.position = position;
	}

	public int getPosition(){
		return position;
	}

	public void setImageBaseVertical(String imageBaseVertical){
		this.imageBaseVertical = imageBaseVertical;
	}

	public String getImageBaseVertical(){
		return imageBaseVertical;
	}

	public void setImageLarge(String imageLarge){
		this.imageLarge = imageLarge;
	}

	public String getImageLarge(){
		return imageLarge;
	}

	public void setTitleUri(String titleUri){
		this.titleUri = titleUri;
	}

	public String getTitleUri(){
		return titleUri;
	}

	public void setImageCleanSquare(String imageCleanSquare){
		this.imageCleanSquare = imageCleanSquare;
	}

	public String getImageCleanSquare(){
		return imageCleanSquare;
	}

	public void setImageLargeAlt(String imageLargeAlt){
		this.imageLargeAlt = imageLargeAlt;
	}

	public String getImageLargeAlt(){
		return imageLargeAlt;
	}
}