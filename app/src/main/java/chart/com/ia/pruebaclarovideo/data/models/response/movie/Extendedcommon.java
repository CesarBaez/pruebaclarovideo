package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;

public class Extendedcommon{

	@SerializedName("keywords")
	private Keywords keywords;

	@SerializedName("genres")
	private Genres genres;

	@SerializedName("roles")
	private Roles roles;

	@SerializedName("format")
	private Format format;

	@SerializedName("media")
	private Media media;

	public void setKeywords(Keywords keywords){
		this.keywords = keywords;
	}

	public Keywords getKeywords(){
		return keywords;
	}

	public void setGenres(Genres genres){
		this.genres = genres;
	}

	public Genres getGenres(){
		return genres;
	}

	public void setRoles(Roles roles){
		this.roles = roles;
	}

	public Roles getRoles(){
		return roles;
	}

	public void setFormat(Format format){
		this.format = format;
	}

	public Format getFormat(){
		return format;
	}

	public void setMedia(Media media){
		this.media = media;
	}

	public Media getMedia(){
		return media;
	}
}