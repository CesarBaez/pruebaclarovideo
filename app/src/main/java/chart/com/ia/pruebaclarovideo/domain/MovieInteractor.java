package chart.com.ia.pruebaclarovideo.domain;

import android.content.Context;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieService;
import rx.Subscription;

public class MovieInteractor {
    private Context context;
    private Subscription subscription;
    private OnMovieListener listener;
    private MovieService service;

    public MovieInteractor(Context context, MovieService service) {
        this.context = context;
        this.service = service;
    }

    public void setListener(OnMovieListener listener) {
        this.listener = listener;
    }

    public void getMovie(String url) {
        subscription = service.getMovie(url).subscribe(movieResponse -> {
            if (listener != null) {
                listener.onDataSent(movieResponse);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });
    }

    public interface OnMovieListener {
        void onServiceError(String mensaje);

        void onDataSent(MovieResponse movieResponse);
    }
}
