package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Rights{

	@SerializedName("end_date")
	private String endDate;

	@SerializedName("start_date")
	private String startDate;

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}
}