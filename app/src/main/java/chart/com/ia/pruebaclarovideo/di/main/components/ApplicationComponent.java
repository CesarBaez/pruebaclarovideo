package chart.com.ia.pruebaclarovideo.di.main.components;

import javax.inject.Singleton;

import chart.com.ia.pruebaclarovideo.di.data.DataModule;
import chart.com.ia.pruebaclarovideo.di.domain.DomainModule;
import chart.com.ia.pruebaclarovideo.di.main.modules.ApplicationModule;
import chart.com.ia.pruebaclarovideo.ui.BillBoard.BillBoardActivity;
import chart.com.ia.pruebaclarovideo.ui.MovieDetail.MovieDetailActivity;
import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, DataModule.class, DomainModule.class})
public interface ApplicationComponent {

    void inject(BillBoardActivity billBoardActivity);

    void inject(MovieDetailActivity movieDetailActivity);
}
