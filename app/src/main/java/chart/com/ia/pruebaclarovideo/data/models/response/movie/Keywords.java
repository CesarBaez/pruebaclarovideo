package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Keywords{

	@SerializedName("keyword")
	private List<String> keyword;

	public void setKeyword(List<String> keyword){
		this.keyword = keyword;
	}

	public List<String> getKeyword(){
		return keyword;
	}
}