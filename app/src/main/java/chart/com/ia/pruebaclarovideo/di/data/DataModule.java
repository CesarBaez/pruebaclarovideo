package chart.com.ia.pruebaclarovideo.di.data;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import chart.com.ia.pruebaclarovideo.data.DataConfiguration;
import chart.com.ia.pruebaclarovideo.data.retrofit.BillBoardClient;
import chart.com.ia.pruebaclarovideo.data.retrofit.MovieClient;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardRetrofitService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieRetrofitService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieService;
import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.schedulers.Schedulers;

@Module
public class DataModule {

    private String baseUrl;

    public DataModule(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Provides
    @Singleton
    DataConfiguration providesDataConfiguration() {
        return new DataConfiguration(baseUrl);
    }

    @Provides
    @Singleton
    OkHttpClient provideOkHttpClient() {
        return new OkHttpClient.Builder()
                .readTimeout(180, TimeUnit.SECONDS)
                .connectTimeout(180, TimeUnit.SECONDS)
                .build();
    }

    @Provides
    @Singleton
    Retrofit providesRetrofit(DataConfiguration dataConfiguration, OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(dataConfiguration.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io()))
                .client(okHttpClient)
                .build();
    }



    @Provides
    @Singleton
    BillBoardService provideBillBoardService(Retrofit retrofit) {
        return new BillBoardClient(retrofit.create(BillBoardRetrofitService.class));
    }

    @Provides
    @Singleton
    MovieService provideMovieService(Retrofit retrofit){
        return new MovieClient(retrofit.create(MovieRetrofitService.class));
    }


}

