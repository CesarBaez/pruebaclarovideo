package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Components{

	@SerializedName("component")
	private List<ComponentItem> component;

	public void setComponent(List<ComponentItem> component){
		this.component = component;
	}

	public List<ComponentItem> getComponent(){
		return component;
	}
}