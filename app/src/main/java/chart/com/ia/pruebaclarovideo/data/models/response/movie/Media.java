package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Media{

	@SerializedName("publishyear")
	private String publishyear;

	@SerializedName("islive")
	private String islive;

	@SerializedName("rollingcreditstimedb")
	private Object rollingcreditstimedb;

	@SerializedName("recorder_technology")
	private RecorderTechnology recorderTechnology;

	@SerializedName("profile")
	private Profile profile;

	@SerializedName("rating")
	private Rating rating;

	@SerializedName("livetype")
	private Object livetype;

	@SerializedName("description_extended")
	private String descriptionExtended;

	@SerializedName("language")
	private Language language;

	@SerializedName("boxoffice")
	private String boxoffice;

	@SerializedName("duration")
	private String duration;

	@SerializedName("originaltitle")
	private String originaltitle;

	@SerializedName("timeshift")
	private Object timeshift;

	@SerializedName("rights")
	private Rights rights;

	@SerializedName("haspreview")
	private String haspreview;

	@SerializedName("countryoforigin")
	private Countryoforigin countryoforigin;

	@SerializedName("proveedor")
	private Proveedor proveedor;

	@SerializedName("liveref")
	private Object liveref;

	@SerializedName("encoder_tecnology")
	private EncoderTecnology encoderTecnology;

	@SerializedName("resource_name")
	private Object resourceName;

	@SerializedName("rollingcreditstime")
	private String rollingcreditstime;

	public void setPublishyear(String publishyear){
		this.publishyear = publishyear;
	}

	public String getPublishyear(){
		return publishyear;
	}

	public void setIslive(String islive){
		this.islive = islive;
	}

	public String getIslive(){
		return islive;
	}

	public void setRollingcreditstimedb(Object rollingcreditstimedb){
		this.rollingcreditstimedb = rollingcreditstimedb;
	}

	public Object getRollingcreditstimedb(){
		return rollingcreditstimedb;
	}

	public void setRecorderTechnology(RecorderTechnology recorderTechnology){
		this.recorderTechnology = recorderTechnology;
	}

	public RecorderTechnology getRecorderTechnology(){
		return recorderTechnology;
	}

	public void setProfile(Profile profile){
		this.profile = profile;
	}

	public Profile getProfile(){
		return profile;
	}

	public void setRating(Rating rating){
		this.rating = rating;
	}

	public Rating getRating(){
		return rating;
	}

	public void setLivetype(Object livetype){
		this.livetype = livetype;
	}

	public Object getLivetype(){
		return livetype;
	}

	public void setDescriptionExtended(String descriptionExtended){
		this.descriptionExtended = descriptionExtended;
	}

	public String getDescriptionExtended(){
		return descriptionExtended;
	}

	public void setLanguage(Language language){
		this.language = language;
	}

	public Language getLanguage(){
		return language;
	}

	public void setBoxoffice(String boxoffice){
		this.boxoffice = boxoffice;
	}

	public String getBoxoffice(){
		return boxoffice;
	}

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setOriginaltitle(String originaltitle){
		this.originaltitle = originaltitle;
	}

	public String getOriginaltitle(){
		return originaltitle;
	}

	public void setTimeshift(Object timeshift){
		this.timeshift = timeshift;
	}

	public Object getTimeshift(){
		return timeshift;
	}

	public void setRights(Rights rights){
		this.rights = rights;
	}

	public Rights getRights(){
		return rights;
	}

	public void setHaspreview(String haspreview){
		this.haspreview = haspreview;
	}

	public String getHaspreview(){
		return haspreview;
	}

	public void setCountryoforigin(Countryoforigin countryoforigin){
		this.countryoforigin = countryoforigin;
	}

	public Countryoforigin getCountryoforigin(){
		return countryoforigin;
	}

	public void setProveedor(Proveedor proveedor){
		this.proveedor = proveedor;
	}

	public Proveedor getProveedor(){
		return proveedor;
	}

	public void setLiveref(Object liveref){
		this.liveref = liveref;
	}

	public Object getLiveref(){
		return liveref;
	}

	public void setEncoderTecnology(EncoderTecnology encoderTecnology){
		this.encoderTecnology = encoderTecnology;
	}

	public EncoderTecnology getEncoderTecnology(){
		return encoderTecnology;
	}

	public void setResourceName(Object resourceName){
		this.resourceName = resourceName;
	}

	public Object getResourceName(){
		return resourceName;
	}

	public void setRollingcreditstime(String rollingcreditstime){
		this.rollingcreditstime = rollingcreditstime;
	}

	public String getRollingcreditstime(){
		return rollingcreditstime;
	}
}