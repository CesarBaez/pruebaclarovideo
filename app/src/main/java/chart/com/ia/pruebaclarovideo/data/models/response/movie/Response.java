package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Response{

	@SerializedName("group")
	private Group group;

	public void setGroup(Group group){
		this.group = group;
	}

	public Group getGroup(){
		return group;
	}
}