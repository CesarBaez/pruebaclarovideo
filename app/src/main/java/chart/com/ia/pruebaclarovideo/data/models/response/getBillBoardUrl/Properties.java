package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Properties{

    @SerializedName("ordenamiento")
    private List<OrdenamientoItem> ordenamiento;

    @SerializedName("byuser")
    private String byuser;

    @SerializedName("id")
    private String id;

    @SerializedName("type")
    private String type;

    @SerializedName("url")
    private String url;
    @SerializedName("small")
    private String small;

    @SerializedName("htmltag")
    private String htmltag;

    @SerializedName("large")
    private String large;

    @SerializedName("link_app_behaviour")
    private String linkAppBehaviour;

    @SerializedName("imgsmall")
    private String imgsmall;

    @SerializedName("imgmedium")
    private String imgmedium;

    @SerializedName("medium")
    private String medium;

    @SerializedName("link_node")
    private String linkNode;

    @SerializedName("link_type")
    private String linkType;

    @SerializedName("imgdefault")
    private String imgdefault;

    @SerializedName("link_special")
    private String linkSpecial;

    @SerializedName("imgextrasmall")
    private String imgextrasmall;

    @SerializedName("link_group")
    private String linkGroup;

    @SerializedName("extrasmall")
    private String extrasmall;

    @SerializedName("imglarge")
    private String imglarge;

    public void setOrdenamiento(List<OrdenamientoItem> ordenamiento){
        this.ordenamiento = ordenamiento;
    }

    public List<OrdenamientoItem> getOrdenamiento(){
        return ordenamiento;
    }

    public void setByuser(String byuser){
        this.byuser = byuser;
    }

    public String getByuser(){
        return byuser;
    }

    public void setId(String id){
        this.id = id;
    }

    public String getId(){
        return id;
    }

    public void setType(String type){
        this.type = type;
    }

    public String getType(){
        return type;
    }

    public void setUrl(String url){
        this.url = url;
    }

    public String getUrl(){
        return url;
    }

    public void setSmall(String small){
        this.small = small;
    }

    public String getSmall(){
        return small;
    }

    public void setHtmltag(String htmltag){
        this.htmltag = htmltag;
    }

    public String getHtmltag(){
        return htmltag;
    }

    public void setLarge(String large){
        this.large = large;
    }

    public String getLarge(){
        return large;
    }

    public void setLinkAppBehaviour(String linkAppBehaviour){
        this.linkAppBehaviour = linkAppBehaviour;
    }

    public String getLinkAppBehaviour(){
        return linkAppBehaviour;
    }

    public void setImgsmall(String imgsmall){
        this.imgsmall = imgsmall;
    }

    public String getImgsmall(){
        return imgsmall;
    }

    public void setImgmedium(String imgmedium){
        this.imgmedium = imgmedium;
    }

    public String getImgmedium(){
        return imgmedium;
    }

    public void setMedium(String medium){
        this.medium = medium;
    }

    public String getMedium(){
        return medium;
    }

    public void setLinkNode(String linkNode){
        this.linkNode = linkNode;
    }

    public String getLinkNode(){
        return linkNode;
    }

    public void setLinkType(String linkType){
        this.linkType = linkType;
    }

    public String getLinkType(){
        return linkType;
    }

    public void setImgdefault(String imgdefault){
        this.imgdefault = imgdefault;
    }

    public String getImgdefault(){
        return imgdefault;
    }

    public void setLinkSpecial(String linkSpecial){
        this.linkSpecial = linkSpecial;
    }

    public String getLinkSpecial(){
        return linkSpecial;
    }

    public void setImgextrasmall(String imgextrasmall){
        this.imgextrasmall = imgextrasmall;
    }

    public String getImgextrasmall(){
        return imgextrasmall;
    }

    public void setLinkGroup(String linkGroup){
        this.linkGroup = linkGroup;
    }

    public String getLinkGroup(){
        return linkGroup;
    }

    public void setExtrasmall(String extrasmall){
        this.extrasmall = extrasmall;
    }

    public String getExtrasmall(){
        return extrasmall;
    }

    public void setImglarge(String imglarge){
        this.imglarge = imglarge;
    }

    public String getImglarge(){
        return imglarge;
    }
}