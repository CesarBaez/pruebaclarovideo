package chart.com.ia.pruebaclarovideo.domain;

import android.content.Context;

import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ResponseGetUrl;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardService;
import rx.Subscription;

public class BillBoardInteractor {
    private Context context;
    private Subscription subscription;
    private onBillBoardListener listener;
    private BillBoardService service;

    public BillBoardInteractor(Context context, BillBoardService service) {
        this.context = context;
        this.service = service;
    }


    public void setListener(onBillBoardListener listener) {
        this.listener = listener;
    }

    public void getBillBoardUrl() {
        subscription = service.getBillBoardUrl().subscribe(response -> {
            if (listener != null) {
                listener.onGetUrlSuccess(response);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });
    }

    public void getBillBoard(String url, String api_level) {
        subscription = service.getBillBoard(url + "/" + api_level).subscribe(billBoardResponse -> {
            if (listener != null) {
                listener.onDataSent(billBoardResponse);
            }
        }, throwable -> {
            if (listener != null) {
                listener.onServiceError(throwable.getMessage());
            }
        });

    }

    public interface onBillBoardListener {
        void onServiceError(String mensaje);

        void onDataSent(ResponseBillBoard billBoardResponse);

        void onGetUrlSuccess(ResponseGetUrl responseGetUrl);
    }

}
