package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class External{

	@SerializedName("gracenote")
	private Gracenote gracenote;

	public void setGracenote(Gracenote gracenote){
		this.gracenote = gracenote;
	}

	public Gracenote getGracenote(){
		return gracenote;
	}
}