package chart.com.ia.pruebaclarovideo.ui.MovieDetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import chart.com.ia.pruebaclarovideo.R;

/**
 * Created by fbaez on 17/10/2017.
 */

public class PlaceholderFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_DATA = "section_data";
    private static final String ARG_SECTION_BOOLEAN = "section_boolean";

    public PlaceholderFragment() {
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static PlaceholderFragment newInstance(Boolean isSynipsis, String data) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_SECTION_DATA, data);
        args.putBoolean(ARG_SECTION_BOOLEAN, isSynipsis);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_pager, container, false);
        TextView title = (TextView) rootView.findViewById(R.id.section_title);
        if (getArguments().getBoolean(ARG_SECTION_BOOLEAN)) {
            title.setText(getString(R.string.sinopsis));
        } else {
            title.setText(getString(R.string.reparto));
        }
        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
        textView.setText(getArguments().getString(ARG_SECTION_DATA));
        return rootView;
    }
}
