package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Roles{

	@SerializedName("role")
	private List<RoleItem> role;

	public void setRole(List<RoleItem> role){
		this.role = role;
	}

	public List<RoleItem> getRole(){
		return role;
	}
}