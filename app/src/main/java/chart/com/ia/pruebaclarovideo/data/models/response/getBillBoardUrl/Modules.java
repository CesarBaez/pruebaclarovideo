package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class Modules{

	@SerializedName("module")
	private List<ModuleItem> module;

	public void setModule(List<ModuleItem> module){
		this.module = module;
	}

	public List<ModuleItem> getModule(){
		return module;
	}
}