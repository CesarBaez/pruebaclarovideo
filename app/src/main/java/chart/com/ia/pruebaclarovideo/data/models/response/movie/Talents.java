package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;
import com.google.gson.annotations.SerializedName;


public class Talents{

	@SerializedName("talent")
	private List<TalentItem> talent;

	public void setTalent(List<TalentItem> talent){
		this.talent = talent;
	}

	public List<TalentItem> getTalent(){
		return talent;
	}
}