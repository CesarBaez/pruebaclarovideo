package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;


import com.google.gson.annotations.SerializedName;

public class ComponentItem{

	@SerializedName("name")
	private String name;

	@SerializedName("type")
	private String type;

	@SerializedName("properties")
	private Properties properties;

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setProperties(Properties properties){
		this.properties = properties;
	}

	public Properties getProperties(){
		return properties;
	}
}