package chart.com.ia.pruebaclarovideo.data.retrofit.services;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import rx.Observable;

public interface MovieService {
    Observable<MovieResponse> getMovie(String url);
}
