package chart.com.ia.pruebaclarovideo.data.models.response.movie;


import com.google.gson.annotations.SerializedName;


public class Format{

	@SerializedName("types")
	private String types;

	@SerializedName("sell_type")
	private String sellType;

	@SerializedName("est")
	private String est;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private String id;

	public void setTypes(String types){
		this.types = types;
	}

	public String getTypes(){
		return types;
	}

	public void setSellType(String sellType){
		this.sellType = sellType;
	}

	public String getSellType(){
		return sellType;
	}

	public void setEst(String est){
		this.est = est;
	}

	public String getEst(){
		return est;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}
}