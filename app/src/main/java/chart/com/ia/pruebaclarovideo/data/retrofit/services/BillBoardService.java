package chart.com.ia.pruebaclarovideo.data.retrofit.services;

import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard.ResponseBillBoard;
import chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl.ResponseGetUrl;
import rx.Observable;

public interface BillBoardService {
    Observable<ResponseGetUrl> getBillBoardUrl();

    Observable<ResponseBillBoard> getBillBoard(String url);
}
