package chart.com.ia.pruebaclarovideo.di.domain;

import android.content.Context;

import javax.inject.Singleton;

import chart.com.ia.pruebaclarovideo.data.retrofit.services.BillBoardService;
import chart.com.ia.pruebaclarovideo.data.retrofit.services.MovieService;
import chart.com.ia.pruebaclarovideo.domain.BillBoardInteractor;
import chart.com.ia.pruebaclarovideo.domain.MovieInteractor;
import dagger.Module;
import dagger.Provides;

@Module
public class DomainModule {

    @Provides
    @Singleton
    BillBoardInteractor provideBillBoardInteractor(BillBoardService service, Context context){
        return new BillBoardInteractor(context, service);
    }

    @Provides
    @Singleton
    MovieInteractor provideMovieInteractor(MovieService service,Context context){
        return new MovieInteractor(context,service);
    }
}
