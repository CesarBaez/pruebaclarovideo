package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;


import com.google.gson.annotations.SerializedName;


public class ResponseGetUrl{

	@SerializedName("msg")
	private String msg;

	@SerializedName("entry")
	private Entry entry;

	@SerializedName("response")
	private Response response;

	@SerializedName("status")
	private String status;

	public void setMsg(String msg){
		this.msg = msg;
	}

	public String getMsg(){
		return msg;
	}

	public void setEntry(Entry entry){
		this.entry = entry;
	}

	public Entry getEntry(){
		return entry;
	}

	public void setResponse(Response response){
		this.response = response;
	}

	public Response getResponse(){
		return response;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}