package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard;


import com.google.gson.annotations.SerializedName;

public class Entry{

	@SerializedName("authpt")
	private String authpt;

	@SerializedName("quantity")
	private String quantity;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("device_model")
	private String deviceModel;

	@SerializedName("level_id")
	private String levelId;

	@SerializedName("format")
	private String format;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("authpn")
	private String authpn;

	@SerializedName("device_category")
	private String deviceCategory;

	@SerializedName("device_manufacturer")
	private String deviceManufacturer;

	@SerializedName("order_way")
	private String orderWay;

	@SerializedName("HKS")
	private String hKS;

	@SerializedName("from")
	private String from;

	@SerializedName("region")
	private String region;

	@SerializedName("order_id")
	private String orderId;

	@SerializedName("node_id")
	private String nodeId;

	public void setAuthpt(String authpt){
		this.authpt = authpt;
	}

	public String getAuthpt(){
		return authpt;
	}

	public void setQuantity(String quantity){
		this.quantity = quantity;
	}

	public String getQuantity(){
		return quantity;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setDeviceModel(String deviceModel){
		this.deviceModel = deviceModel;
	}

	public String getDeviceModel(){
		return deviceModel;
	}

	public void setLevelId(String levelId){
		this.levelId = levelId;
	}

	public String getLevelId(){
		return levelId;
	}

	public void setFormat(String format){
		this.format = format;
	}

	public String getFormat(){
		return format;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setAuthpn(String authpn){
		this.authpn = authpn;
	}

	public String getAuthpn(){
		return authpn;
	}

	public void setDeviceCategory(String deviceCategory){
		this.deviceCategory = deviceCategory;
	}

	public String getDeviceCategory(){
		return deviceCategory;
	}

	public void setDeviceManufacturer(String deviceManufacturer){
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getDeviceManufacturer(){
		return deviceManufacturer;
	}

	public void setOrderWay(String orderWay){
		this.orderWay = orderWay;
	}

	public String getOrderWay(){
		return orderWay;
	}

	public void setHKS(String hKS){
		this.hKS = hKS;
	}

	public String getHKS(){
		return hKS;
	}

	public void setFrom(String from){
		this.from = from;
	}

	public String getFrom(){
		return from;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setOrderId(String orderId){
		this.orderId = orderId;
	}

	public String getOrderId(){
		return orderId;
	}

	public void setNodeId(String nodeId){
		this.nodeId = nodeId;
	}

	public String getNodeId(){
		return nodeId;
	}
}