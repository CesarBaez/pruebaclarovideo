package chart.com.ia.pruebaclarovideo.ui.MovieDetail;

import javax.inject.Inject;

import chart.com.ia.pruebaclarovideo.data.models.response.movie.MovieResponse;
import chart.com.ia.pruebaclarovideo.domain.MovieInteractor;
import ia.com.commons.view.BasePresenter;

public class MoviePresenter extends BasePresenter<MoviePresenter.View> implements MovieInteractor.OnMovieListener {

    private MovieInteractor movieInteractor;

    public void getMovie(String url) {
        if (isViewAttached()) {
            getView().showLoading();
            movieInteractor.getMovie(url);
        }

    }

    @Inject
    public MoviePresenter(MovieInteractor movieInteractor) {
        this.movieInteractor = movieInteractor;
        this.movieInteractor.setListener(this);
    }

    @Override
    public void onServiceError(String mensaje) {
        if (isViewAttached()){
            getView().hideLoading();
            getView().onMovieServiceError(mensaje);
        }
    }

    @Override
    public void onDataSent(MovieResponse movieResponse) {
        if (isViewAttached()) {
            getView().hideLoading();
            getView().onMovieServiceSuccess(movieResponse);
        }
    }

    public interface View extends BasePresenter.View {
        void onMovieServiceSuccess(MovieResponse movieResponse);

        void onMovieServiceError(String error);

        @Override
        void showLoading();

        @Override
        void hideLoading();
    }
}
