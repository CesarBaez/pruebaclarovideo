package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;

import com.google.gson.annotations.SerializedName;

public class OrdenamientoItem{

	@SerializedName("label")
	private String label;

	@SerializedName("order")
	private String order;

	public void setLabel(String label){
		this.label = label;
	}

	public String getLabel(){
		return label;
	}

	public void setOrder(String order){
		this.order = order;
	}

	public String getOrder(){
		return order;
	}
}
