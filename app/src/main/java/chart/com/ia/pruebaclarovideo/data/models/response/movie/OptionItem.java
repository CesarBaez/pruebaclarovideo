package chart.com.ia.pruebaclarovideo.data.models.response.movie;

import java.util.List;

import com.google.gson.annotations.SerializedName;


public class OptionItem{

	@SerializedName("credits_start_time")
	private String creditsStartTime;

	@SerializedName("content_id")
	private String contentId;

	@SerializedName("label_large")
	private String labelLarge;

	@SerializedName("intro_start_time")
	private Object introStartTime;

	@SerializedName("option_name")
	private String optionName;

	@SerializedName("label_short")
	private String labelShort;

	@SerializedName("encodes")
	private List<String> encodes;

	@SerializedName("current_content")
	private String currentContent;

	@SerializedName("group_id")
	private String groupId;

	@SerializedName("subtitle")
	private Object subtitle;

	@SerializedName("option_id")
	private String optionId;

	@SerializedName("intro_finish_time")
	private Object introFinishTime;

	@SerializedName("audio")
	private String audio;

	@SerializedName("id")
	private String id;

	@SerializedName("desc")
	private String desc;

	public void setCreditsStartTime(String creditsStartTime){
		this.creditsStartTime = creditsStartTime;
	}

	public String getCreditsStartTime(){
		return creditsStartTime;
	}

	public void setContentId(String contentId){
		this.contentId = contentId;
	}

	public String getContentId(){
		return contentId;
	}

	public void setLabelLarge(String labelLarge){
		this.labelLarge = labelLarge;
	}

	public String getLabelLarge(){
		return labelLarge;
	}

	public void setIntroStartTime(Object introStartTime){
		this.introStartTime = introStartTime;
	}

	public Object getIntroStartTime(){
		return introStartTime;
	}

	public void setOptionName(String optionName){
		this.optionName = optionName;
	}

	public String getOptionName(){
		return optionName;
	}

	public void setLabelShort(String labelShort){
		this.labelShort = labelShort;
	}

	public String getLabelShort(){
		return labelShort;
	}

	public void setEncodes(List<String> encodes){
		this.encodes = encodes;
	}

	public List<String> getEncodes(){
		return encodes;
	}

	public void setCurrentContent(String currentContent){
		this.currentContent = currentContent;
	}

	public String getCurrentContent(){
		return currentContent;
	}

	public void setGroupId(String groupId){
		this.groupId = groupId;
	}

	public String getGroupId(){
		return groupId;
	}

	public void setSubtitle(Object subtitle){
		this.subtitle = subtitle;
	}

	public Object getSubtitle(){
		return subtitle;
	}

	public void setOptionId(String optionId){
		this.optionId = optionId;
	}

	public String getOptionId(){
		return optionId;
	}

	public void setIntroFinishTime(Object introFinishTime){
		this.introFinishTime = introFinishTime;
	}

	public Object getIntroFinishTime(){
		return introFinishTime;
	}

	public void setAudio(String audio){
		this.audio = audio;
	}

	public String getAudio(){
		return audio;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setDesc(String desc){
		this.desc = desc;
	}

	public String getDesc(){
		return desc;
	}
}