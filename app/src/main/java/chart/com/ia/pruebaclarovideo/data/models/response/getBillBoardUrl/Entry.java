package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;


import com.google.gson.annotations.SerializedName;


public class Entry{

	@SerializedName("authpt")
	private String authpt;

	@SerializedName("HKS")
	private String hKS;

	@SerializedName("node")
	private String node;

	@SerializedName("device_id")
	private String deviceId;

	@SerializedName("device_model")
	private String deviceModel;

	@SerializedName("format")
	private String format;

	@SerializedName("device_type")
	private String deviceType;

	@SerializedName("authpn")
	private String authpn;

	@SerializedName("api_version")
	private String apiVersion;

	@SerializedName("region")
	private String region;

	@SerializedName("device_category")
	private String deviceCategory;

	@SerializedName("device_manufacturer")
	private String deviceManufacturer;

	public void setAuthpt(String authpt){
		this.authpt = authpt;
	}

	public String getAuthpt(){
		return authpt;
	}

	public void setHKS(String hKS){
		this.hKS = hKS;
	}

	public String getHKS(){
		return hKS;
	}

	public void setNode(String node){
		this.node = node;
	}

	public String getNode(){
		return node;
	}

	public void setDeviceId(String deviceId){
		this.deviceId = deviceId;
	}

	public String getDeviceId(){
		return deviceId;
	}

	public void setDeviceModel(String deviceModel){
		this.deviceModel = deviceModel;
	}

	public String getDeviceModel(){
		return deviceModel;
	}

	public void setFormat(String format){
		this.format = format;
	}

	public String getFormat(){
		return format;
	}

	public void setDeviceType(String deviceType){
		this.deviceType = deviceType;
	}

	public String getDeviceType(){
		return deviceType;
	}

	public void setAuthpn(String authpn){
		this.authpn = authpn;
	}

	public String getAuthpn(){
		return authpn;
	}

	public void setApiVersion(String apiVersion){
		this.apiVersion = apiVersion;
	}

	public String getApiVersion(){
		return apiVersion;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setDeviceCategory(String deviceCategory){
		this.deviceCategory = deviceCategory;
	}

	public String getDeviceCategory(){
		return deviceCategory;
	}

	public void setDeviceManufacturer(String deviceManufacturer){
		this.deviceManufacturer = deviceManufacturer;
	}

	public String getDeviceManufacturer(){
		return deviceManufacturer;
	}
}