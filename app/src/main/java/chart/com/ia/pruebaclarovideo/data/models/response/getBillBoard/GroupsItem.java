package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoard;


import com.google.gson.annotations.SerializedName;


public class GroupsItem{

	@SerializedName("date")
	private String date;

	@SerializedName("preview")
	private String preview;

	@SerializedName("year")
	private String year;

	@SerializedName("live_ref")
	private String liveRef;

	@SerializedName("image_medium")
	private String imageMedium;

	@SerializedName("season_number")
	private String seasonNumber;

	@SerializedName("description")
	private String description;

	@SerializedName("live_type")
	private String liveType;

	@SerializedName("title")
	private String title;

	@SerializedName("rating_code")
	private String ratingCode;

	@SerializedName("title_episode")
	private String titleEpisode;

	@SerializedName("title_original")
	private String titleOriginal;

	@SerializedName("duration")
	private String duration;

	@SerializedName("image_small")
	private String imageSmall;

	@SerializedName("image_still")
	private String imageStill;

	@SerializedName("episode_number")
	private String episodeNumber;

	@SerializedName("format_types")
	private String formatTypes;

	@SerializedName("live_enabled")
	private String liveEnabled;

	@SerializedName("votes_average")
	private int votesAverage;

	@SerializedName("id")
	private String id;

	@SerializedName("image_large")
	private String imageLarge;

	@SerializedName("image_background")
	private String imageBackground;

	@SerializedName("title_uri")
	private String titleUri;

	@SerializedName("description_large")
	private String descriptionLarge;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setPreview(String preview){
		this.preview = preview;
	}

	public String getPreview(){
		return preview;
	}

	public void setYear(String year){
		this.year = year;
	}

	public String getYear(){
		return year;
	}

	public void setLiveRef(String liveRef){
		this.liveRef = liveRef;
	}

	public String getLiveRef(){
		return liveRef;
	}

	public void setImageMedium(String imageMedium){
		this.imageMedium = imageMedium;
	}

	public String getImageMedium(){
		return imageMedium;
	}

	public void setSeasonNumber(String seasonNumber){
		this.seasonNumber = seasonNumber;
	}

	public String getSeasonNumber(){
		return seasonNumber;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setLiveType(String liveType){
		this.liveType = liveType;
	}

	public String getLiveType(){
		return liveType;
	}

	public void setTitle(String title){
		this.title = title;
	}

	public String getTitle(){
		return title;
	}

	public void setRatingCode(String ratingCode){
		this.ratingCode = ratingCode;
	}

	public String getRatingCode(){
		return ratingCode;
	}

	public void setTitleEpisode(String titleEpisode){
		this.titleEpisode = titleEpisode;
	}

	public String getTitleEpisode(){
		return titleEpisode;
	}

	public void setTitleOriginal(String titleOriginal){
		this.titleOriginal = titleOriginal;
	}

	public String getTitleOriginal(){
		return titleOriginal;
	}

	public void setDuration(String duration){
		this.duration = duration;
	}

	public String getDuration(){
		return duration;
	}

	public void setImageSmall(String imageSmall){
		this.imageSmall = imageSmall;
	}

	public String getImageSmall(){
		return imageSmall;
	}

	public void setImageStill(String imageStill){
		this.imageStill = imageStill;
	}

	public String getImageStill(){
		return imageStill;
	}

	public void setEpisodeNumber(String episodeNumber){
		this.episodeNumber = episodeNumber;
	}

	public String getEpisodeNumber(){
		return episodeNumber;
	}

	public void setFormatTypes(String formatTypes){
		this.formatTypes = formatTypes;
	}

	public String getFormatTypes(){
		return formatTypes;
	}

	public void setLiveEnabled(String liveEnabled){
		this.liveEnabled = liveEnabled;
	}

	public String getLiveEnabled(){
		return liveEnabled;
	}

	public void setVotesAverage(int votesAverage){
		this.votesAverage = votesAverage;
	}

	public int getVotesAverage(){
		return votesAverage;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getId(){
		return id;
	}

	public void setImageLarge(String imageLarge){
		this.imageLarge = imageLarge;
	}

	public String getImageLarge(){
		return imageLarge;
	}

	public void setImageBackground(String imageBackground){
		this.imageBackground = imageBackground;
	}

	public String getImageBackground(){
		return imageBackground;
	}

	public void setTitleUri(String titleUri){
		this.titleUri = titleUri;
	}

	public String getTitleUri(){
		return titleUri;
	}

	public void setDescriptionLarge(String descriptionLarge){
		this.descriptionLarge = descriptionLarge;
	}

	public String getDescriptionLarge(){
		return descriptionLarge;
	}
}