package chart.com.ia.pruebaclarovideo.data.models.response.getBillBoardUrl;


import com.google.gson.annotations.SerializedName;


public class ModuleItem{

	@SerializedName("components")
	private Components components;

	@SerializedName("name")
	private String name;

	@SerializedName("type")
	private String type;

	public void setComponents(Components components){
		this.components = components;
	}

	public Components getComponents(){
		return components;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}
}