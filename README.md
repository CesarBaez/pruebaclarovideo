# PruebaClaroVideo

El repositorio incluye el apk de la aplicación has click sobre el siguiente hipervinculo para descargarlo.
[Descargar APK](https://bitbucket.org/CesarBaez/pruebaclarovideo/raw/ce77adc6ce43199402ba67f7c080ee5aa642fc00/app-debug.apk)

## Descripción

Esta prueba consiste en replicar dos de las vistas actuales de Claro video, se necesitará
revisar que endpoints se están consumiendo para traer la lista de películas disponibles y
el detalle de la película seleccionada.

Requerimientos técnicos generales:

*  Usar las mejores prácticas en el desarrollo de aplicaciones Android.
*  El desarrollo deberá incluir pruebas unitarias.
*  Subir la prueba a un repositorio público. ¿Gitflow?
*  Redactar archivo README con las instrucciones mínimas indispensables para correr el proyecto y las pruebas unitarias.
*  En el repositorio se debería incluir apk de la última versión estable en la raíz del repositorio y deberá ser señalado en README.


## El proyecto
Este proyecto está generado para

* Android Studio 3.0 (Mayo 2017)
* con Gradle 4.4 
* nivel de API mínimo SDK 21 (Lollipop)


## La arquitectura

* El proyecto se desarrollo utilizando la arquitectura MVP + Dagger [Ver mas de esta arquitectura aqui.](https://android.jlelse.eu/mvp-dagger-2-rx-clean-modern-android-app-code-74f63c9a6f2f)
* Tenemos un modulo de commos donde generamos clases abstractas de la Base de las Actividades, Fragments y Presenters.


**IMPORTANTE:** Cuando se corra el proyecto es necesario que el dispocitivo tenga acceso a internet de lo contarrio la aplicación no podra mostrar los contenidos
, en su lugar solo se observara un mensage del error a la petición del servivio.

## Librerias utilizadas

* [Dagger](http://square.github.io/dagger/)
* [Picasso](http://square.github.io/picasso/)
* [Butterknife](http://jakewharton.github.io/butterknife/)
* [Retrofit](http://square.github.io/retrofit/)
* [Gson](https://github.com/google/gson)
* [RxJava](https://github.com/ReactiveX/RxJava)



